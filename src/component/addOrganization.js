import React,{useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { Types, creators } from '../redux/Type';

function AddOrganization() {
    const [typeSelected, setType] = useState('Clinic');
    const [orgName, setOrgName] = useState('');
    const types = useSelector(state=>state.typeReducer);
    
    const dispatch = useDispatch();


   const handleSubmit = (e) => {
        e.preventDefault();
        let predata = {
            type: typeSelected,
            name: orgName,
            id: Date.now()
        }
        dispatch(creators.storeOrganizationRequest(predata));
        setOrgName('');
    }

    return (
        <form onSubmit={e=>handleSubmit(e)}>
            <div class="form-group">
                <label for="type">Organization Type:</label>
                <select class="form-control" id="type" onChange={(e)=>setType(e.target.value)} required>
                    <option label='select'></option>
                    {
                        types.org_type.map(vl=><option selected={vl.name==typeSelected}>{vl.name}</option>)
                    }

                </select>
            </div>
            <div class="form-group">
                <label for="name">{typeSelected } Name</label>
                <input class="form-control" type="text" id="name" value={orgName} onChange={e=>setOrgName(e.target.value)} required/>
            </div>
            <div class="form-group">
                <button class="btn btn-default" type="submit">Save</button>
            </div>
        </form>
    )
}

export default AddOrganization;