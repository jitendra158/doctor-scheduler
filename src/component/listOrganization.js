import React,{useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {creators} from '../redux/Type';


const ListOrganization = () => {
    const organizations = useSelector(state=>state.typeReducer.organizations);
    const dispatch = useDispatch();
    return (
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <td>Sr.</td>
                    <td>Name</td>
                    <td>Type</td>
                </tr>
            </thead>
            <tbody>
                {organizations.map((org,index)=>
                    <tr style={{cursor:'pointer'}} onClick={()=>dispatch(creators.selectOrganization(org))}>
                        <td>{index+1}</td>
                        <td>{org.name}</td>
                        <td>{org.type}</td>
                    </tr>)}
            </tbody>
        </table>
    )
}

export default ListOrganization;