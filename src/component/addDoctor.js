import React,{useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { Types, creators } from '../redux/Doctor';

function AddDoctor() {
    const [slotSelected, setSlot] = useState(null);
    const [docName, setDocName] = useState('');
    const [speciality, setspeciality] = useState('');

    const doctors = useSelector(state=>state.doctorReducer);
    const days = useSelector(state=>state.timeslotReducer.days);
    const selectedorg = useSelector(state=>state.typeReducer.selected_organization);
    const dispatch = useDispatch();


   const handleSubmit = (e) => {
        e.preventDefault();
        let predata = {
            orgid : selectedorg.id,
            speciality: speciality,
            time: slotSelected,
            name: docName,
            id: Date.now()
        }
        dispatch(creators.storeDoctorRequest(predata));
        setSlot(null);
        setDocName('');
        setspeciality('');
    }

    return (
        <form onSubmit={e=>handleSubmit(e)}>
            <div class="form-group">
                <label for="slot">Select Day:</label>
                <select class="form-control" id="slot" onChange={(e)=>setSlot(e.target.value)} required>
                    <option label='select'></option>
                    {
                        days.map(vl=><option selected={vl.name==slotSelected}>{vl.name}</option>)
                    }

                </select>
            </div>
            <div class="form-group">
                <label for="name">Doctor's Name</label>
                <input class="form-control" type="text" id="name" value={docName} onChange={e=>setDocName(e.target.value)} required/>
            </div>
            <div class="form-group">
                <label for="name">Speciality</label>
                <input class="form-control" type="text" id="name" value={speciality} onChange={e=>setspeciality(e.target.value)} required/>
            </div>
            <div class="form-group">
                <button class="btn btn-default" type="submit">Save</button>
            </div>
        </form>
    )
}

export default AddDoctor;