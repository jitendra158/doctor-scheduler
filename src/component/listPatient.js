import React,{useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {creators} from '../redux/Patients';
import _ from 'lodash';


const ListDoctors = () => {
    const pats = useSelector(state=>state.patientReducer);
    const selectedorg = useSelector(state=>state.typeReducer.selected_organization);
    const dispatch = useDispatch();
    let patients = _.filter(pats.patients,['orgid',selectedorg.id]);
    return (
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <td>Patient ID</td>
                    <td>Name</td>
                    <td>Age</td>
                    <td>Symptomps</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                {patients.map((org,index)=>
                    <tr>
                        <td>{org.id}</td>
                        <td>{org.name}</td>
                        <td>{org.age}</td>
                        <td>{org.symptomps}</td>
                        <td>
                            <ul>
                               {
                                   !pats.edit_patient ?
                                   <>
                                <li style={{cursor:'pointer',color:'blue'}} onClick={()=>dispatch(creators.startEditPatientRequest(org))}>Edit</li>
                                <li  style={{cursor:'pointer',color:'red'}} onClick={()=>dispatch(creators.removePatientRequest(org))}>Remove</li>
                                </> : ''}
                                
                            </ul>
                        </td>
                    </tr>)}
            </tbody>
        </table>
    )
}

export default ListDoctors;