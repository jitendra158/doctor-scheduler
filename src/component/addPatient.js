import React,{useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { Types, creators } from '../redux/Patients';

function AddPatient() {
    const [age, setAge] = useState(10);
    const [patientName, setPatientName] = useState('');
    const [symptomps, setSymptomps] = useState('');

    const selectedorg = useSelector(state=>state.typeReducer.selected_organization);
    const dispatch = useDispatch();


   const handleSubmit = (e) => {
        e.preventDefault();
        let predata = {
            orgid : selectedorg.id,
            symptomps: symptomps,
            age : age,
            name: patientName,
            id: Date.now()
        }
        
        dispatch(creators.storePatientRequest(predata));
        setAge(10);
        setPatientName('');
        setSymptomps('');
    }

    return (
        <form onSubmit={e=>handleSubmit(e)}>
            <div class="form-group">
                <label for="name">Patient's Name</label>
                <input class="form-control" type="text" id="name" value={patientName} onChange={e=>setPatientName(e.target.value)} required/>
            </div>
            <div class="form-group">
                <label for="age">Age(in years)</label>
                <input class="form-control" type="text" id="age" value={age} onChange={e=>setAge(e.target.value)} required/>
            </div>
            <div class="form-group">
                <label for="symp">Symptomps</label>
                <input class="form-control" type="text" id="symp" value={symptomps} onChange={e=>setSymptomps(e.target.value)} required/>
            </div>
            <div class="form-group">
                <button class="btn btn-default" type="submit">Save</button>
            </div>
        </form>
    )
}

export default AddPatient;