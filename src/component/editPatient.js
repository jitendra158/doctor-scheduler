import React,{useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import { Types, creators } from '../redux/Patients';

function EditPatient() {
    const selectedpatient = useSelector(state=>state.patientReducer.selected_patient);
    const [age, setAge] = useState(selectedpatient.age);
    const [patientName, setPatientName] = useState(selectedpatient.name);
    const [symptomps, setSymptomps] = useState(selectedpatient.symptomps);

    
    const dispatch = useDispatch();


   const handleSubmit = (e) => {
        e.preventDefault();
        let predata = {
            orgid : selectedpatient.orgid,
            symptomps: symptomps,
            age : age,
            name: patientName,
            id: selectedpatient.id
        }
        
        dispatch(creators.editPatientRequest(predata));
    }

    const handleCancel = (e) => {
        e.preventDefault();
        dispatch(creators.cancelEditPatientRequest());
    }

    return (
        <form onSubmit={e=>handleSubmit(e)}>
            <div class="form-group">
                <label for="name">Patient's Name</label>
                <input class="form-control" type="text" id="name" value={patientName} onChange={e=>setPatientName(e.target.value)} required/>
            </div>
            <div class="form-group">
                <label for="age">Age(in years)</label>
                <input class="form-control" type="text" id="age" value={age} onChange={e=>setAge(e.target.value)} required/>
            </div>
            <div class="form-group">
                <label for="symp">Symptomps</label>
                <input class="form-control" type="text" id="symp" value={symptomps} onChange={e=>setSymptomps(e.target.value)} required/>
            </div>
            <div class="form-group">
                <button class="btn btn-default" type="submit">Update</button>
                <button class="btn btn-default" onClick={(e)=>handleCancel(e)}>Cancel</button>
            </div>
        </form>
    )
}

export default EditPatient;