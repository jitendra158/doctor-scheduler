import React,{useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import _ from 'lodash';


const ListDoctors = () => {
    const docs = useSelector(state=>state.doctorReducer.doctors);
    const selectedorg = useSelector(state=>state.typeReducer.selected_organization);

    let doctors = _.filter(docs,['orgid',selectedorg.id]);

    return (
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td>Sr.</td>
                    <td>Name</td>
                    <td>Speciality</td>
                    <td>Time</td>
                </tr>
            </thead>
            <tbody>
                {doctors.map((org,index)=>
                    <tr>
                        <td>{index+1}</td>
                        <td>{org.name}</td>
                        <td>{org.speciality}</td>
                        <td>{org.time}</td>
                    </tr>)}
            </tbody>
        </table>
    )
}

export default ListDoctors;