import React,{useState, useEffect} from 'react';
import {useSelector} from 'react-redux';


import AddOrganization from './addOrganization';
import ListOrganization from './listOrganization';

import AddDoctors from './addDoctor';
import ListDoctors from './listDoctor';

import AddPatient from './addPatient';
import EditPatient from './editPatient';
import ListPatient from './listPatient';

function Entry() {
    const org = useSelector(state=>state.typeReducer);
    const editpatient = useSelector(state=>state.patientReducer.edit_patient)
  return (
      <table class="table">
          <thead>
              <tr>
                  <td>Hospitals/Clinic</td>
                  <td>Doctors</td>
                  <td>Patients</td>
                  <td></td>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td>
                    <AddOrganization />
                    <ListOrganization />
                  </td>
                  <td>
                      {
                          org.is_org_selected ? <>
                            <AddDoctors />
                            <ListDoctors />
                          </>
                          :
                          <h4>No Hospital / Clinic selected</h4>
                      }
                      
                  </td>
                  <td>
                    {
                          org.is_org_selected ? <>
                            {editpatient ? <EditPatient />: <AddPatient /> }
                            <ListPatient />
                          </>
                          :
                          <h4>No Hospital / Clinic selected</h4>
                      }
                  </td>
              </tr>
          </tbody>
          
        </table>
  );
}

export default Entry;
