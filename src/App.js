import React,{useState, useEffect} from 'react';
import {Provider} from 'react-redux';
import './App.css';


import store from './redux/store';

import Main from './component';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="App-header">
          <p>Doctor's Scheduler</p>
        </header>
          <Main />
        </div>
    </Provider>
  );
}

export default App;
