import Immutable from 'seamless-immutable';
import _ from 'lodash';
export const Types = {
    //add patient
    ADD_PATIENT : 'ADD_PATIENT',
    STORE_PATIENT_REQUEST : 'STORE_PATIENT_REQUEST',
    REMOVE_PATIENT_REQUEST : 'REMOVE_PATIENT_REQUEST',
    START_EDIT_PATIENT_REQUEST : 'START_EDIT_PATIENT_REQUEST',
    CANCEL_EDIT_PATIENT_REQUEST : 'CANCEL_EDIT_PATIENT_REQUEST',
    EDIT_PATIENT_REQUEST : 'EDIT_PATIENT_REQUEST'
}

export const initialState = Immutable({
    patients : [],
    add_patient :false,
    store_patient_success : false,
    edit_patient : false,
    selected_patient : null,
    remove_patient : false
});


export const creators = {
    addPatient : () => ({
        type : Types.ADD_PATIENT
    }),

    storePatientRequest: (data) => ({
        type : Types.STORE_PATIENT_REQUEST,
        req : data,
    }),

    startEditPatientRequest: (data) => ({
        type : Types.START_EDIT_PATIENT_REQUEST,
        req : data,
    }),

    cancelEditPatientRequest: () => ({
        type : Types.CANCEL_EDIT_PATIENT_REQUEST,
    }),

    editPatientRequest: (data) => ({
        type : Types.EDIT_PATIENT_REQUEST,
        req : data,
    }),

    removePatientRequest: (data) => ({
        type : Types.REMOVE_PATIENT_REQUEST,
        req : data,
    }),

}


const patientReducer = (state=initialState, action) => {
    
    switch (action.type) {
        case Types.ADD_PATIENT : return {
            ...state,
            add_patient : true,
        }

        case Types.STORE_PATIENT_REQUEST : {
            return{
            ...state,
            store_patient_success : true,
            patients : [...state.patients,action.req]
            }
           }
        

        case Types.START_EDIT_PATIENT_REQUEST : {
            return{
            ...state,
            edit_patient : true,
            selected_patient : action.req
            }
        }

        case Types.CANCEL_EDIT_PATIENT_REQUEST : {
            return{
            ...state,
            edit_patient : false,
            selected_patient : null
            }
        }

        case Types.EDIT_PATIENT_REQUEST : {
            var patients = state.patients;
            var fin = _.map(patients,function(n) {
                if(n.id==action.req.id){
                    return action.req;
                }else{
                    return n;
                }
              })
            return{
            ...state,
            edit_patient : false,
            patients : fin
            }
        }

        case Types.REMOVE_PATIENT_REQUEST : {
            var patients = state.patients;
            var fin = _.remove(patients,function(n) {
                return n != action.req;
              })
            return{
            ...state,
            store_patient_success : true,
            patients : fin
            }
        }

        default : return state;
    }
}


export default patientReducer;