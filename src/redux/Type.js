import Immutable from 'seamless-immutable';
export const Types = {
    //add organization
    ADD_ORGANIZATION : 'ADD_ORGANIZATION',
    STORE_ORGANIZATION_REQUEST : 'STORE_ORGANIZATION_REQUEST',
    SELECT_ORGANIZATION : 'SELECT_ORGANIZATION'
}

export const initialState = Immutable({
    org_type : [{
        name: 'Clinic'
    },
    {
        name: 'Hospital'  
    }],
    organizations : [],
    add_organization :false,
    store_organization_success : false,
    is_org_selected : false,
    selected_organization : null,
});


export const creators = {
    addOrganiation : () => ({
        type : Types.ADD_ORGANIZATION
    }),

    storeOrganizationRequest: (data) => ({
        type : Types.STORE_ORGANIZATION_REQUEST,
        req : data,
    }),

    selectOrganization : (data) => ({
        type : Types.SELECT_ORGANIZATION,
        req : data,
    })

}


const typeReducer = (state=initialState, action) => {
    
    switch (action.type) {
        case Types.ADD_ORGANIZATION : return {
            ...state,
            add_organization : true,
        }

        case Types.STORE_ORGANIZATION_REQUEST : {
            
            
            return{
            ...state,
            store_organization_success : true,
            organizations : [...state.organizations,action.req]
            }
           }
        
        case Types.SELECT_ORGANIZATION : {
            return {
                ...state,
                is_org_selected : true,
                selected_organization : action.req
            }
        }

        default : return state;
    }
}


export default typeReducer;