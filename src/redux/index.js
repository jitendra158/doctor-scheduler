import { combineReducers } from 'redux';

import typeReducer from './Type';
import doctorReducer from './Doctor';
import timeslotReducer from './Timeslots';
import patientReducer from './Patients';

export default combineReducers({
    typeReducer,
    doctorReducer,
    timeslotReducer,
    patientReducer
})