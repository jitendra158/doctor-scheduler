import Immutable from 'seamless-immutable';
export const Types = {
    //add doctor
    ADD_DOCTOR : 'ADD_DOCTOR',
    STORE_DOCTOR_REQUEST : 'STORE_DOCTOR_REQUEST',
}

export const initialState = Immutable({
    doctors : [],
    add_doctor :false,
    store_doctor_success : false,
});


export const creators = {
    addDoctor : () => ({
        type : Types.ADD_DOCTOR
    }),

    storeDoctorRequest: (data) => ({
        type : Types.STORE_DOCTOR_REQUEST,
        req : data,
    }),

}


const doctorReducer = (state=initialState, action) => {
    
    switch (action.type) {
        case Types.ADD_DOCTOR : return {
            ...state,
            add_doctor : true,
        }

        case Types.STORE_DOCTOR_REQUEST : {
            
            
            return{
            ...state,
            store_doctor_success : true,
            doctors : [...state.doctors,action.req]
            }
           }

        default : return state;
    }
}


export default doctorReducer;